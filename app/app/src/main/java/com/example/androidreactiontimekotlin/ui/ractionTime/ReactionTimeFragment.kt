package com.example.androidreactiontimekotlin.ui.ractionTime

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.androidreactiontimekotlin.R
import com.example.androidreactiontimekotlin.databinding.FragmentReactionTimeBinding
import kotlinx.android.synthetic.main.fragment_reaction_time.*

class ReactionTimeFragment : Fragment(){

    private lateinit var viewModel: ReactionTimeViewModel

    private lateinit var binding: FragmentReactionTimeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_reaction_time, container, false)
        viewModel = ViewModelProviders.of(this).get(ReactionTimeViewModel::class.java)
        binding.reactionViewModel = viewModel
        binding.lifecycleOwner = this
        registerObserver()
        return binding.root
    }

    private fun registerObserver() {
        viewModel.backgroundColor.observe(this, Observer<Int> {
            constraintLayout.setBackgroundColor(it)
        })
    }
}

