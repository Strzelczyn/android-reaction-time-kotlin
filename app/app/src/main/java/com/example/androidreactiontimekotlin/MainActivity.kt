package com.example.androidreactiontimekotlin

import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import com.example.androidreactiontimekotlin.ui.ractionTime.ReactionTimeViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (ReactionTimeViewModel.endTime == 0L && ReactionTimeViewModel.startTime != 0L) {
            ReactionTimeViewModel.endTime = System.currentTimeMillis()
        }
        return super.onTouchEvent(event)
    }
}
