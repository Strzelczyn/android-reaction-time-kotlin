package com.example.androidreactiontimekotlin.ui.ractionTime

import android.graphics.Color
import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

private const val CLICK_TIME_OUT = 3000L

class ReactionTimeViewModel : ViewModel() {

    val startButtonVisibility = MutableLiveData<Boolean>(true)

    val restatButtonVisibility = MutableLiveData<Boolean>(false)

    val text = MutableLiveData<String>()

    val backgroundColor = MutableLiveData<Int>()

    companion object {

        var endTime: Long = 0

        var startTime: Long = 0
    }

    fun Start() {
        startButtonVisibility.value = false
        val timeToStart = Random.nextLong(400, 1400)
        backgroundColor.value = Color.RED
        text.value = "Tap when screen is green"
        Handler().postDelayed({
            text.value = ""
            backgroundColor.value = Color.GREEN
            startTime = System.currentTimeMillis()
        }, timeToStart)
        Handler().postDelayed({
            backgroundColor.value = Color.RED
            restatButtonVisibility.value = true
            if (endTime > startTime) {
                text.value = "Your reaction time is ${endTime - startTime} ms"
            } else {
                text.value = "Time to click has ended"
            }
        }, CLICK_TIME_OUT)
    }

    fun Restart() {
        startButtonVisibility.value = true
        restatButtonVisibility.value = false
        backgroundColor.value = Color.WHITE
        text.value = ""
        endTime = 0
        startTime = 0
    }
}
